package ru.easton.tm.publisher;

import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.exception.TaskNotFoundException;
import ru.easton.tm.listener.Listener;

public interface Publisher {
    void addListener(Listener listener);
    void deleteListener(Listener listener);
    void listen(String command) throws ProjectNotFoundException, TaskNotFoundException;
    void start();
}
