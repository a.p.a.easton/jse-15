package ru.easton.tm.service;

import ru.easton.tm.entity.Project;
import ru.easton.tm.entity.Task;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private static ProjectTaskService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final TaskRepository taskRepository = TaskRepository.getInstance();

    private ProjectTaskService(){
    }

    public static ProjectTaskService getInstance(){
        if (instance == null){
            instance = new ProjectTaskService();
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        if(projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if(projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Project project = projectRepository.findById(projectId);
        if(project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        if(!(project.getUserId().equals(userId) && task.getUserId().equals(userId))) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Project project = projectRepository.findById(projectId);
        if(project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId, final Long userId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Project project = projectRepository.findById(projectId);
        final Task idTask = taskRepository.findById(taskId);
        if(!(project.getUserId().equals(userId) && idTask.getUserId().equals(userId))) return null;
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Project project = projectRepository.findById(projectId);
        final Task idTask = taskRepository.findById(taskId);
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
