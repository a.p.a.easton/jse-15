package ru.easton.tm.service;

import ru.easton.tm.entity.Project;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.repository.ProjectRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractService{

    private static ProjectService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final UserService userService = UserService.getInstance();

    private ProjectService(){
    }

    public static ProjectService getInstance(){
        if (instance == null){
            instance = new ProjectService();
        }
        return instance;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description) throws ProjectNotFoundException{
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        if (description == null || description.isEmpty()) throw new ProjectNotFoundException("Project's description can't be empty");
        return projectRepository.update(id, name, description);
    }

    public void clear(final Long userId) throws ProjectNotFoundException {
        projectRepository.clear(userId);
    }

    public List<Project> findByUserId(final Long userId) {
        return projectRepository.findByUserId(userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        return projectRepository.findByIndex(index, userId);
    }

    public Project findByIndex(final int index) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        return projectRepository.findByIndex(index);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException{
        if(name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        return projectRepository.findByName(name, userId);
    }

    public void removeByIndex(int index, final Long userId) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        projectRepository.removeByIndex(index, userId);
    }

    public void removeByName(String name, Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        projectRepository.removeByName(name, userId);
    }

    public void removeById(final Long id) throws ProjectNotFoundException{
        if (id == null) throw new ProjectNotFoundException("Id can't be null");
        projectRepository.removeById(id);
    }

    public int createProject(){
        final Long userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final String description = scanner.nextLine();
        create(name, description, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully created");
        return 0;
    }

    public int viewProjectByIndex() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("ENTER, PROJECT INDEX:");
        int index = -1;
        try{
            index = Integer.parseInt(scanner.nextLine()) - 1;
        }
        catch (NumberFormatException e){
            System.out.println("ERROR, INCORRECT INDEX");
        }
        final Project project = findByIndex(index, userId);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final List<Project> projects = findByName(name, userId);
        viewProject(projects);
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = findByIndex(index, userId);
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        logger.info("Project was successfully updated");
        return 0;
    }

    public int removeProjectByName() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        removeByName(name, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        removeById(id);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectByIndex() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX: ");
        final int index = scanner.nextInt() - 1;
        removeByIndex(index, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int clearProject() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CLEAR PROJECT]");
        clear(userId);
        System.out.println("[OK]");
        logger.info("Projects was successfully cleared");
        return 0;
    }

    public int listProject() throws ProjectNotFoundException {
        final Long userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = findByUserId(userId);
        if(projects.isEmpty()) throw new ProjectNotFoundException("You don't have any projects");
        Collections.sort(projects, Comparator.comparing(Project::getName));
        int index = 1;
        for(final Project project: projects){
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        return 0;
    }

    private void viewProject(final Project project){
        if(project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private void viewProject(final List<Project> projects){
        for(final Project project: projects) {
            System.out.println("[VIEW PROJECT]");
            System.out.println("ID: " + project.getId());
            System.out.println("NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("[OK]");
        }
    }

}
