package ru.easton.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.controller.ProjectController;

import java.util.Scanner;

public abstract class AbstractService {

    protected final Scanner scanner = new Scanner(System.in);

    protected static final Logger logger = LogManager.getLogger(AbstractService.class);

    protected boolean checkAuthentication(final Long userId){
        if(userId == null){
            System.out.println("Please, log in or registry.");
            return true;
        }
        return false;
    }
}
