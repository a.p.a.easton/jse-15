package ru.easton.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.entity.Project;
import ru.easton.tm.exception.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProjectRepository {

    private static ProjectRepository instance = null;

    private final List<Project> projects = new ArrayList<>();

    private final HashMap<String, List<Project>> projectsMap = new HashMap<>();

    private final Logger logger = LogManager.getLogger(ProjectRepository.class);

    private ProjectRepository(){
    }

    public static ProjectRepository getInstance(){
        if (instance == null){
            instance = new ProjectRepository();
        }
        return instance;
    }

    private void addProjectToMap(final Project project){
        projectsMap.putIfAbsent(project.getName(), new ArrayList<>());
        projectsMap.get(project.getName()).add(project);
    }

    public Project create(final String name){
        final Project project = new Project(name);
        projects.add(project);
        addProjectToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId){
        logger.trace("create --> name: {}, description: {}, userId: {}", name, description, userId);
        final Project project = new Project(name, description);
        project.setUserId(userId);
        projects.add(project);
        addProjectToMap(project);
        logger.trace("create --> {}", project);
        return project;
    }

    public Project update(final Long id, final String name, final String description){
        logger.trace("update --> id: {}, name: {}, description: {}", id, name, description);
        final Project project = findById(id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        logger.trace("update --> {}", project);
        return project;
    }

    public void clear(final Long userId) throws ProjectNotFoundException {
        logger.trace("clear --> userId: {}", userId);
        List<Project> userProjects = findByUserId(userId);
        if(userProjects.isEmpty()) throw new ProjectNotFoundException("You don't have any project");
        for(final Project project: userProjects){
            projectsMap.get(project.getName()).remove(project);
            if(projectsMap.get(project.getName()).isEmpty())
                projectsMap.remove(project.getName());
            projects.remove(project);
        }
    }

    public List<Project> findByUserId(final Long userId) {
        logger.trace("findByUserId --> userId: {}", userId);
        List<Project> userProjects = new ArrayList<>();
        for(final Project project: projects){
            if(project.getUserId() == null) continue;
            if(project.getUserId().equals(userId)) userProjects.add(project);
        }
        logger.trace("findByUserId --> {}", userProjects);
        return userProjects;
    }

    public List<Project> findAll(){
        return projects;
    }

    public Project findByIndex(int index, Long userId) throws ProjectNotFoundException {
        logger.trace("findByIndex --> index: {}, userId: {}", index, userId);
        List<Project> userProjects = findByUserId(userId);
        if(index > userProjects.size() - 1) throw new ProjectNotFoundException("Entered index more than number of your projects");
        logger.trace("findByIndex --> {}", userProjects.get(index));
        return userProjects.get(index);
    }

    public Project findByIndex(int index){
        if(index > projects.size() - 1) return null;
        return projects.get(index);
    }

    public Project findByName(final String name, final List<Project> userProjects){
        logger.trace("findByName --> name: {}, userProjects: {}", name, userProjects);
        for(final Project project: userProjects){
            if(project.getName().equals(name)) return project;
        }
        return null;
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException{
        logger.trace("findByName --> name: {}, userId: {}", name, userId);
        List<Project> projectsByName = projectsMap.get(name);
        if(projectsByName == null) throw new ProjectNotFoundException("Projects with entered name not found");
        List<Project> result = new ArrayList<>();
        for(final Project project: projectsByName){
            if(project.getUserId()==null) continue;
            if(project.getUserId().equals(userId)) result.add(project);
        }
        if(result.isEmpty()) throw new ProjectNotFoundException(String.format("You don't have projects with name \"%s\"", name));
        logger.trace("findByName --> {}", result);
        return result;
    }

    public Project findById(final Long id){
        for(final Project project: projects){
            if(project.getId().equals(id)) return project;
        }
        return null;
    }

    public void removeByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        logger.trace("removeByIndex --> index: {}, userId: {}", index, userId);
        final Project project = findByIndex(index, userId);
        if(project==null) throw new ProjectNotFoundException(String.format("You don't have projects with index \"%s\"", index));
        projects.remove(project);
    }

    public void removeByName(final String name, Long userId) throws ProjectNotFoundException {
        logger.trace("removeByName --> name: {}, userId: {}", name, userId);
        final List<Project> userProjects = findByUserId(userId);
        final Project project = findByName(name, userProjects);
        if(project==null) throw new ProjectNotFoundException(String.format("You don't have projects with name \"%s\"", name));
        projects.remove(project);
    }

    public void removeById(final Long id) throws ProjectNotFoundException {
        logger.trace("removeById --> id: {}", id);
        final Project project = findById(id);
        if(project==null) throw new ProjectNotFoundException(String.format("You don't have projects with id \"%s\"", id));
        projects.remove(project);
    }

}
