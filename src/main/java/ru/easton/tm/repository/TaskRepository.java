package ru.easton.tm.repository;

import ru.easton.tm.entity.Task;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.exception.TaskNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskRepository {

    private static TaskRepository instance = null;

    private final List<Task> tasks = new ArrayList<>();

    private final HashMap<String, List<Task>> tasksMap = new HashMap<>();

    private TaskRepository(){
    }

    public static TaskRepository getInstance(){
        if (instance == null){
            instance = new TaskRepository();
        }
        return instance;
    }

    public void addTaskToMap(final Task task){
        tasksMap.putIfAbsent(task.getName(), new ArrayList<>());
        tasksMap.get(task.getName()).add(task);
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        addTaskToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId){
        final Task task = new Task(name, description);
        task.setUserId(userId);
        tasks.add(task);
        addTaskToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description){
        final Task task = findById(id);
        if(task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public void clear(final Long userId) throws TaskNotFoundException {
        List<Task> userTasks = findByUserId(userId);
        if(userTasks.isEmpty()) throw new TaskNotFoundException("You don't have any project");
        for(final Task task: userTasks) {
            tasksMap.get(task.getName()).remove(task);
            if(tasksMap.get(task.getName()).size() == 0)
                tasksMap.remove(task.getName());
            tasks.remove(task);
        }
    }

    public List<Task> findByUserId(final Long userId) {
        List<Task> userTasks = new ArrayList<>();
        for(final Task task: tasks){
            if(task.getUserId() == null) continue;
            if(task.getUserId().equals(userId)) userTasks.add(task);
        }
        return userTasks;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task findByIndex(int index, final Long userId) throws TaskNotFoundException{
        List<Task> userTasks = findByUserId(userId);
        if(index > userTasks.size() - 1) throw new TaskNotFoundException("Entered index more than number of your tasks");
        return userTasks.get(index);
    }

    public Task findByIndex(int index){
        if(index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name, final List<Task> userTasks){
        for(final Task task: userTasks){
            if(task.getName().equals(name)) return task;
        }
        return null;
    }

    public List<Task> findByName(final String name, final Long userId) throws TaskNotFoundException{
        List<Task> tasksByName = tasksMap.get(name);
        if(tasksByName == null) throw new TaskNotFoundException("Tasks with entered name not found");
        List<Task> result = new ArrayList<>();
        for(final Task task: tasksByName){
            if(task.getUserId()==null) continue;
            if(task.getUserId().equals(userId)) result.add(task);
        }
        if(result.isEmpty()) throw new TaskNotFoundException(String.format("You don't have tasks with name \"%s\"", name));
        return result;
    }

    public Task findById(final Long id){
        for(final Task task: tasks){
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            final Long idUser = task.getUserId();
            if(idProject == null) continue;
            if(idUser == null) continue;
            if(idProject.equals(projectId) && idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByProjectId(final Long projectId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            if(idProject == null) continue;
            if(idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id){
        for(final Task task: tasks){
            final Long idProject = task.getProjectId();
            if(idProject == null) continue;
            if(!idProject.equals(projectId)) continue;
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public void removeByName(final String name, final Long userId) throws TaskNotFoundException{
        final List<Task> userTasks = findByUserId(userId);
        if(userTasks.isEmpty()) throw new TaskNotFoundException("You don't have any tasks");
        final Task task = findByName(name, userTasks);
        if(task==null) throw new TaskNotFoundException(String.format("You don't have tasks with name \"%s\"", name));
        tasks.remove(task);
    }

    public void removeById(final Long id) throws TaskNotFoundException {
        final Task task = findById(id);
        if(task==null) throw new TaskNotFoundException(String.format("You don't have tasks with id \"%s\"", id));
        tasks.remove(task);
    }

    public void removeByIndex(final int index, final Long userId) throws TaskNotFoundException {
        final Task task = findByIndex(index, userId);
        if(task==null) throw new TaskNotFoundException(String.format("You don't have tasks with index \"%s\"", index));
        tasks.remove(task);
    }

}
