package ru.easton.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.entity.Project;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.service.ProjectService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService = ProjectService.getInstance();

    private static final Logger logger = LogManager.getLogger(ProjectController.class);

    public int createProject(final Long userId){
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final String description = scanner.nextLine();
        projectService.create(name, description, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully created");
        return 0;
    }

    public int viewProjectByIndex(final Long userId) throws ProjectNotFoundException {
        if(checkAuthentication(userId)) return 0;
        System.out.println("ENTER, PROJECT INDEX:");
        int index = -1;
        try{
            index = Integer.parseInt(scanner.nextLine()) - 1;
        }
        catch (NumberFormatException e){
            System.out.println("ERROR, INCORRECT INDEX");
        }
        final Project project = projectService.findByIndex(index, userId);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName(final Long userId) throws ProjectNotFoundException {
        if(checkAuthentication(userId)) return 0;
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final List<Project> projects = projectService.findByName(name, userId);
        viewProject(projects);
        return 0;
    }

    public int updateProjectByIndex(final Long userId) throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index, userId);
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        logger.info("Project was successfully updated");
        return 0;
    }

    public int removeProjectByName(Long userId) throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        projectService.removeByName(name, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        projectService.removeById(id);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectByIndex(final Long userId) throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX: ");
        final int index = scanner.nextInt() - 1;
        projectService.removeByIndex(index, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int clearProject(final Long userId) throws ProjectNotFoundException {
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CLEAR PROJECT]");
        projectService.clear(userId);
        System.out.println("[OK]");
        logger.info("Projects was successfully cleared");
        return 0;
    }

    public int listProject(final Long userId) throws ProjectNotFoundException {
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findByUserId(userId);
        if(projects.isEmpty()) throw new ProjectNotFoundException("You don't have any projects");
        Collections.sort(projects, Comparator.comparing(Project::getName));
        int index = 1;
        for(final Project project: projects){
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        return 0;
    }

    private void viewProject(final Project project){
        if(project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private void viewProject(final List<Project> projects){
        for(final Project project: projects) {
            System.out.println("[VIEW PROJECT]");
            System.out.println("ID: " + project.getId());
            System.out.println("NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("[OK]");
        }
    }

}
