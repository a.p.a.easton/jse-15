package ru.easton.tm;

import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.listener.Listener;
import ru.easton.tm.listener.ListenerProjectImpl;
import ru.easton.tm.listener.ListenerTaskImpl;
import ru.easton.tm.listener.ListenerUserImpl;
import ru.easton.tm.publisher.Publisher;
import ru.easton.tm.publisher.PublisherImpl;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;
import ru.easton.tm.service.*;

public class App {

    static {
        User test = UserService.getInstance().create("test", "test", Role.USER);
        User admin = UserService.getInstance().create("admin", "admin", Role.ADMIN);
        ProjectService.getInstance().create("DEMO PROJECT 1", "DEMO PROJECT 1", test.getId());
        ProjectService.getInstance().create("DEMO PROJECT 2", "DEMO PROJECT 2", admin.getId());
        ProjectService.getInstance().create("DEMO PROJECT 3", "DEMO PROJECT 3", test.getId());
        TaskService.getInstance().create("TEST TASK 1", "TEST TASK 1", test.getId());
        TaskService.getInstance().create("TEST TASK 2", "TEST TASK 2", admin.getId());
        TaskService.getInstance().create("TEST TASK 3", "TEST TASK 3", test.getId());
    }

    public static void main(String[] args) {
        Publisher publisher = new PublisherImpl();
        Listener listenerUser = new ListenerUserImpl();
        Listener listenerProject = new ListenerProjectImpl();
        Listener listenerTask = new ListenerTaskImpl();
        publisher.addListener(listenerUser);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        publisher.start();
    }

    public ProjectRepository getProjectRepository(){
        return ProjectRepository.getInstance();
    }

    public TaskRepository getTaskRepository(){
        return TaskRepository.getInstance();
    }

    public ProjectTaskService getProjectTaskService(){
        return ProjectTaskService.getInstance();
    }
}
