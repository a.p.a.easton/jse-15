package ru.easton.tm.listener;

import lombok.SneakyThrows;
import ru.easton.tm.service.SystemService;
import ru.easton.tm.service.TaskService;

import static ru.easton.tm.constant.TerminalConst.*;

public class ListenerTaskImpl implements Listener{

    private final TaskService taskService = TaskService.getInstance();

    private final SystemService systemService = SystemService.getInstance();

    @SneakyThrows
    @Override
    public int listen(String command) {
        switch (command){
            case TASK_CREATE: return taskService.createTask();
            case TASK_CLEAR: return taskService.clearTask();
            case TASK_LIST: return taskService.listTask();
            case TASK_VIEW_BY_INDEX: return taskService.viewTaskByIndex();
            case TASK_VIEW_BY_NAME: return taskService.viewTaskByName();
            case TASK_REMOVE_BY_NAME: return taskService.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskService.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskService.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskService.updateTaskByIndex();
            case TASK_LIST_BY_PROJECT_ID: return taskService.listTasksByProjectId();
            case TASK_ADD_TO_PROJECT_BY_ID: return taskService.addTaskToProjectById();
            case TASK_REMOVE_FROM_PROJECT_BY_ID: return taskService.removeTaskFromProjectById();
            default: return systemService.displayError();
        }
    }
}
