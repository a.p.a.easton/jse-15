package ru.easton.tm.listener;

public interface Listener {
    int listen(final String command);
}
