package ru.easton.tm.listener;

import lombok.SneakyThrows;
import ru.easton.tm.service.ProjectService;
import ru.easton.tm.service.SystemService;

import static ru.easton.tm.constant.TerminalConst.*;

public class ListenerProjectImpl implements Listener{

    private final ProjectService projectService = ProjectService.getInstance();

    private final SystemService systemService = SystemService.getInstance();

    @SneakyThrows
    @Override
    public int listen(String command) {
        switch (command) {
            case PROJECT_CREATE: return projectService.createProject();
            case PROJECT_CLEAR: return projectService.clearProject();
            case PROJECT_LIST: return projectService.listProject();
            case PROJECT_VIEW_BY_INDEX: return projectService.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME: return projectService.viewProjectByName();
            case PROJECT_REMOVE_BY_NAME: return projectService.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectService.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectService.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectService.updateProjectByIndex();
            default: return systemService.displayError();
        }
    }

}
