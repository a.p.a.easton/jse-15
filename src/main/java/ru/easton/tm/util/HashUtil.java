package ru.easton.tm.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

    public static String md5(final String s) {
        try {
            byte[] bytesOfMessage = s.getBytes("UTF-8");
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digest = md5.digest(bytesOfMessage);
            String hash = new BigInteger(1, digest).toString(16);
            return hash;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

}
