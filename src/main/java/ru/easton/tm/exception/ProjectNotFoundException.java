package ru.easton.tm.exception;

public class ProjectNotFoundException extends Exception{

    public ProjectNotFoundException(String message){
        super(message);
    }

}
