package ru.easton.tm.exception;

public class TaskNotFoundException extends Exception{

    public TaskNotFoundException(String message){
        super(message);
    }

}
